const fs = require('fs');
const path = require('path');
const readFromFilepath = path.resolve(__dirname, 'lipsum.txt')
const containerPath = path.resolve(__dirname, 'filenames.txt')

function readFile(filepath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filepath, (err, data) => {
            if (err) {
                reject(err)
            } else {
                data = data.toString();
                resolve(data);
            }
        })
    });
}

function writeFile(newfilepath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(newfilepath, data, (err) => {
            if (err) {
                reject(err)
            } else {
                console.log(`finsihed writing in ${path.basename(newfilepath)}`)
                resolve(newfilepath)
            }
        });
    });
}
function writeFileNamesInAFile(newfilepath, containerPath) {
    let filename = path.parse(newfilepath).name;
    return new Promise((resolve, reject) => {
        fs.writeFile(containerPath, filename, (err) => {
            if (err) {
                reject(err)
            } else {
                console.log(`finsihed writing in ${path.basename(containerPath)}`)
                resolve(newfilepath);
            }
        });
    });

}
function appendFile(newfilepath, containerPath) {
    let filename = path.parse(newfilepath).name;
    return new Promise((resolve, reject) => {
        fs.appendFile(containerPath, '\n' + filename, 'utf8', (err) => {
            if (err) {
                reject(err);
            } else {
                console.log(`${filename} appended in ${path.basename(containerPath)}`)
                resolve(newfilepath)
            }
        })
    })
}


function getDataInLowerCaseAndSplitInSentences(data) {
    data = data.toLowerCase()
    data = data.split(/[\.\n]+/g).filter((str) => str !== ' ').join('\n');
    return data;
}

function getLowerCaseSorted(data) {
    data = data.split('\n').sort().join('\n');
    return data;
}

function deleteFile(directory, filename) {
    return new Promise((resolve, reject) => {
        fs.unlink(path.join(directory, filename), err => {
            if (err) {
                reject(err);
            } else {
                resolve(`deleted ${path.basename(filename)}`)
            }
        });
    })
}


function deleteAllNewFiles(directory, filePathOfFilenamesData) {
    return readFile(filePathOfFilenamesData)
        .then((filenames) => {
            filenames = filenames.split('\n')
                .reduce((acc, val) => ({ ...acc, [val]: val }), {})
            return new Promise((resolve, reject) => {
                fs.readdir(directory, (err, files) => {
                    if (err) {
                        reject(err)
                    } else {
                        for (const file of files) {
                            let filename = path.parse(file).name;
                            if (filenames.hasOwnProperty(filename)) {
                                resolve(deleteFile(directory, file))
                            }
                        }
                    }
                })
            })
        })
}




function problem2(readFromFilepath) {
    readFile(readFromFilepath)
        .then((data) => {
            data = data.toUpperCase();
            return data;
        })
        .then((data) => {
            let newfilepath = path.resolve(__dirname, 'dataInUpperCase.txt');
            return writeFile(newfilepath, data)
        })
        .then((newfilepath) => {
            return Promise.resolve(newfilepath)
            .then((newfilepath) =>writeFileNamesInAFile(newfilepath, containerPath))
        })
        .then((newfilepath) => readFile(newfilepath))
        .then((data) => {
            let newfilepath = path.resolve(__dirname, 'dataInLowerCase.txt');
            data = getDataInLowerCaseAndSplitInSentences(data);
            return writeFile(newfilepath, data)
        })
        .then((newfilepath) => appendFile(newfilepath, containerPath))
        .then((newfilepath) => readFile(newfilepath))
        .then((data) => {
            let newfilepath = path.resolve(__dirname, 'sortedData.txt');
            data = getLowerCaseSorted(data);
            return writeFile(newfilepath, data)
        })
        .then((newfilepath) => appendFile(newfilepath, containerPath))
        .then((newfilepath) => {
            let dirPath = path.dirname(newfilepath);
            return deleteAllNewFiles(dirPath, containerPath);
        })
        .then(() => console.log('All files deleted'))
        .catch(err => console.error(err))
}

problem2(readFromFilepath)
