const fs = require('fs');
const path = require('path');
const files = [1, 2, 3, 4, 5, 6];
const folderpath = path.resolve(__dirname, 'files');


function makeFolder(folderPath) {
    return new Promise((resolve, reject) => {
        fs.mkdir(folderPath, function (err) {
            if (err) {
                reject('failed to create directory');
            } else {
                console.log(`new folder ${path.basename(folderPath)} created`)
                resolve()
            }
        });
    })
}

function deleteFolder(folderPath) {
    return new Promise((resolve, reject) => {
        fs.rmdir(folderPath, function (err) {
            if (err) {
                reject(err);
            } else {
                resolve("Successfully removed the empty directory!");
            }
        })
    })
}

function writeFile(filepath, data) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fs.writeFile(filepath, data, (err) => {
                if (err) {
                    reject(err)
                } else {
                    console.log(`${filepath} created`)
                    resolve()
                }
            })
        }, 2000)
    })
}

function deleteFile(filePath) {
    console.log('deleting file...')
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (err) => {
            if (err) {
                reject('error writing file', err);
            } else {
                console.log(`deleted file ${path.basename(filePath)}`)
                resolve(filePath);
            }
        })
    })
}

function createFiles(files, folderPath) {
    let p = Promise.resolve(makeFolder(folderPath));
    files.forEach(function (filename) {
        p = p.then((data) => {
            return writeFile(`${folderPath}/${filename}.txt`, filename.toString())
        })
    })
    p = p
        .then(() => { return Promise.resolve('All Files created') })
        .catch((err) => { console.error(err) })
    return p
}

function problem1(files, folderpath) {
    createFiles(files, './files')
        .then((msg) => {
            console.log(msg)
            return Promise.resolve();
        })
        .then(() => {
            let promises = [];
            files.forEach((filename) => {
                promises.push(deleteFile(`${folderpath}/${filename}.txt`))
            })
            return Promise.all(promises)
        })
        .then((promises) => deleteFolder(path.dirname(promises[0])))
        .then(() => console.log('Deleted the folder'))
        .catch((err)=>console.log(err))
}
problem1(files,folderpath)
